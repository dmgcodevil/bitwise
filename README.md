# bitwise

## byte to a binary string

```scala
def toBinaryString(byte: Byte): String = Integer.toBinaryString((byte & 0xFF) + 0x100).substring(1)
```

& 0xFF basically converts a signed byte to an unsigned integer. For example, -129, like you said, is 
represented by 11111111111111111111111110000001. In this case, you basically want the first (least significant) 
8 bits, so you AND (&) it with 0xFF (00000000000000000000000011111111), 
effectively cleaning the 1's to the left that we don't care about, leaving out just 10000001.